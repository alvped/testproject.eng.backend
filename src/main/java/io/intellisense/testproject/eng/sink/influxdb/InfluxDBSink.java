package io.intellisense.testproject.eng.sink.influxdb;

import io.intellisense.testproject.eng.model.DataPoint;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.accumulators.Accumulator;
import org.apache.flink.api.common.accumulators.IntCounter;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.influxdb.BatchOptions;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;

import java.util.concurrent.TimeUnit;

@Slf4j
@RequiredArgsConstructor
public class InfluxDBSink<T extends DataPoint> extends RichSinkFunction<T> {

    transient InfluxDB influxDB;

    final ParameterTool configProperties;

    // cluster metrics
    final Accumulator recordsIn = new IntCounter(0);
    final Accumulator recordsOut = new IntCounter(0);

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        getRuntimeContext().addAccumulator(getClass().getSimpleName() + "-recordsIn", recordsIn);
        getRuntimeContext().addAccumulator(getClass().getSimpleName() + "-recordsOut", recordsOut);
        influxDB = InfluxDBFactory.connect(
                configProperties.getRequired("influxdb.url"),
                configProperties.getRequired("influxdb.username"),
                configProperties.getRequired("influxdb.password"));
        final String dbname = configProperties.getRequired("influxdb.dbName");
        influxDB.setDatabase(dbname);
        influxDB.enableBatch(BatchOptions.DEFAULTS);
    }

    @Override
    public void close() throws Exception {
        influxDB.flush();
        influxDB.close();
        super.close();
    }

    @Override
    public void invoke(T dataPoint, Context context) {
        recordsIn.add(1);
        try {
            // It seems is not working with this version of influxDB
            // see https://github.com/influxdata/influxdb-java/issues/586
            // I change to manual mapping as a quick fix
            final Point point = Point.measurement("datapoints")
                    .addField("value", dataPoint.getValue())
                    .time(dataPoint.getTimestamp().toEpochMilli(), TimeUnit.MILLISECONDS)
                    .tag("sensor", dataPoint.getSensor())
                    .tag("anomalous-score", String.valueOf(dataPoint.getAnomalousScore()))
                    .build();
            influxDB.write(point);
            recordsOut.add(1);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
}