package io.intellisense.testproject.eng.function;

import io.intellisense.testproject.eng.datasource.CsvDatasource;
import io.intellisense.testproject.eng.model.DataPoint;
import org.apache.flink.api.java.CollectionEnvironment;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.table.sources.CsvTableSource;
import org.apache.flink.types.Row;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CsvRowToDataPointFlatMapTest {

    @Test
    void shouldGenerate10DataPointsWellFormed() throws Exception {
        DataSet<Row> dataSet = getRowDataSet("sensor-data-test.csv");
        List<DataPoint> dataPoints = dataSet.flatMap(new CsvRowToDataPointFlatMap()).collect();
        assertEquals(expectedDataPoints(), dataPoints);
    }

    @Test
    void shouldParseProperlyMultipleLines() throws Exception {
        DataSet<Row> dataSet = getRowDataSet("sensor-data-test-multiple-lines.csv");
        List<DataPoint> dataPoints = dataSet.flatMap(new CsvRowToDataPointFlatMap()).collect();
        assertEquals(30, dataPoints.size());
    }

    private DataSet<Row> getRowDataSet(String s) {
        final ExecutionEnvironment env = new CollectionEnvironment();
        CsvTableSource csvSource = CsvDatasource.of(s).getCsvSource();
        return csvSource.getDataSet(env);
    }

    List<DataPoint> expectedDataPoints() {
        Instant instant = Instant.parse("2017-01-01T00:00:00Z");
        return Arrays.asList(
                DataPointObjectMother.fromInstant(instant).sensor("Sensor-1").value(215.3042922990786).build(),
                DataPointObjectMother.fromInstant(instant).sensor("Sensor-2").value(82.03962790902443).build(),
                DataPointObjectMother.fromInstant(instant).sensor("Sensor-3").value(81.13454381102596).build(),
                DataPointObjectMother.fromInstant(instant).sensor("Sensor-4").value(77.16378009052926).build(),
                DataPointObjectMother.fromInstant(instant).sensor("Sensor-5").value(16.593020507830083).build(),
                DataPointObjectMother.fromInstant(instant).sensor("Sensor-6").value(68.31273786568367).build(),
                DataPointObjectMother.fromInstant(instant).sensor("Sensor-7").value(3.2984100009032624).build(),
                DataPointObjectMother.fromInstant(instant).sensor("Sensor-8").value(0.7005611028156791).build(),
                DataPointObjectMother.fromInstant(instant).sensor("Sensor-9").value(132.944761501043).build(),
                DataPointObjectMother.fromInstant(instant).sensor("Sensor-10").value(15.7369838498432).build()
        );
    }


}