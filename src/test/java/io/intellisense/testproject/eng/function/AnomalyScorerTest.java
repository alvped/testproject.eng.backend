package io.intellisense.testproject.eng.function;

import io.intellisense.testproject.eng.model.DataPoint;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AnomalyScorerTest {

    AnomalyScorer scorer = AnomalyScorer.builder().firstQuartile(1.0).thirdQuartile(2.0).build();

    @Test
    void shouldComputeThresholdsProperly() {
        assertEquals(-0.5, scorer.getLowOutlierThreshold(), 0.001);
        assertEquals(5.0, scorer.getHighOutlierThreshold(), 0.001);
    }

    @ParameterizedTest
    @ValueSource(doubles = {-1, -.7, -.51})
    void shouldScoreWithZeroLowOutliers(double value) {
        DataPoint lowOutlier = DataPointObjectMother.dataPointWithValue(value);
        scorer.score(lowOutlier);
        assertEquals(0.0, lowOutlier.getAnomalousScore(), 0.0001);
    }

    @ParameterizedTest
    @ValueSource(doubles = {-.4999, 4.999, 2, 3})
    void shouldScoreWithZeroPointFiveNormalPoints(double value) {
        DataPoint lowOutlier = DataPointObjectMother.dataPointWithValue(value);
        scorer.score(lowOutlier);
        assertEquals(0.5, lowOutlier.getAnomalousScore(), 0.0001);
    }

    @ParameterizedTest
    @ValueSource(doubles = {5.0099, 6, 7})
    void shouldScoreWithOneHighOutliers(double value) {
        DataPoint lowOutlier = DataPointObjectMother.dataPointWithValue(value);
        scorer.score(lowOutlier);
        assertEquals(1.0, lowOutlier.getAnomalousScore(), 0.0001);
    }
}