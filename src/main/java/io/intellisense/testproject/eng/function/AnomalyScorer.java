package io.intellisense.testproject.eng.function;

import io.intellisense.testproject.eng.model.DataPoint;
import lombok.Builder;
import lombok.Data;

@Data
public class AnomalyScorer {
    private final double lowOutlierThreshold;
    private final double highOutlierThreshold;

    @Builder
    public AnomalyScorer(double firstQuartile, double thirdQuartile) {
        double iqr = thirdQuartile - firstQuartile;
        this.lowOutlierThreshold = computeLowOutlierThreshold(firstQuartile, iqr);
        this.highOutlierThreshold = computeHighOutlierThreshold(thirdQuartile, iqr);
    }

    public void score(DataPoint dataPointToBeScored) {
        double score = anomalousScoreFor(dataPointToBeScored);
        dataPointToBeScored.setAnomalousScore(score);
    }

    private double anomalousScoreFor(DataPoint dataPointToBeScored) {
        double value = dataPointToBeScored.getValue();
        if (value < lowOutlierThreshold) {
            return 0.0;
        } else if (value >= lowOutlierThreshold && value < highOutlierThreshold) {
            return 0.5;
        }
        return 1.0;
    }

    private double computeLowOutlierThreshold(double firstQuartile, double iqr) {
        return firstQuartile - 1.5 * iqr;
    }

    private double computeHighOutlierThreshold(double thirdQuartile, double iqr) {
        return thirdQuartile + 3 * iqr;
    }
}
