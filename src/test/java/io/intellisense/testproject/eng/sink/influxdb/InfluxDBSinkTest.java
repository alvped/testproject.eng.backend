package io.intellisense.testproject.eng.sink.influxdb;

import io.intellisense.testproject.eng.function.DataPointObjectMother;
import io.intellisense.testproject.eng.model.DataPoint;
import io.intellisense.testproject.eng.utils.SinkFactory;
import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.accumulators.*;
import org.apache.flink.api.common.cache.DistributedCache;
import org.apache.flink.api.common.externalresource.ExternalResourceInfo;
import org.apache.flink.api.common.functions.BroadcastVariableInitializer;
import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.api.common.state.*;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.metrics.MetricGroup;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.awaitility.Awaitility;
import org.influxdb.InfluxDB;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.InfluxDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Testcontainers
class InfluxDBSinkTest {

    @Container
    public InfluxDBContainer influxDbContainer =
            new InfluxDBContainer(DockerImageName.parse("influxdb:1.8"))
                    .withAdmin("admin")
                    .withAdminPassword("admin")
                    .withDatabase("anomaly_detection");

    @Test
    public void itShouldWriteOneElementToInfluxDBAndIncreaseMetrics() throws Exception {
        InfluxDB influxDB = influxDbContainer.getNewInfluxDB();
        int influxDBRunningPort = influxDbContainer.getMappedPort(8086);

        InfluxDBSink<DataPoint> sink = SinkFactory.createInfluxDbSink(influxDBRunningPort);
        sink.setRuntimeContext(getRuntimeContext());
        sink.open(new Configuration());

        assertEquals(0, sink.recordsIn.getLocalValue());
        assertEquals(0, sink.recordsOut.getLocalValue());

        DataPoint dataPoint = DataPointObjectMother.dataPointWithValue(1.0);
        dataPoint.setAnomalousScore(0.5);

        sink.invoke(dataPoint, contextStub());

        assertEquals(1, sink.recordsIn.getLocalValue());
        assertEquals(1, sink.recordsOut.getLocalValue());

        Awaitility.await().atMost(10, TimeUnit.SECONDS).untilAsserted(() -> {
            QueryResult queryResult = influxDB.query(new Query("SELECT * FROM datapoints", "anomaly_detection"));
            assertNotNull(queryResult);
            assertNotNull(queryResult.getResults().get(0).getSeries());
        });

        sink.close();
        influxDB.close();
    }

    private RuntimeContext getRuntimeContext() {
        return new RuntimeContext() {
            @Override
            public String getTaskName() {
                return null;
            }

            @Override
            public MetricGroup getMetricGroup() {
                return null;
            }

            @Override
            public int getNumberOfParallelSubtasks() {
                return 0;
            }

            @Override
            public int getMaxNumberOfParallelSubtasks() {
                return 0;
            }

            @Override
            public int getIndexOfThisSubtask() {
                return 0;
            }

            @Override
            public int getAttemptNumber() {
                return 0;
            }

            @Override
            public String getTaskNameWithSubtasks() {
                return null;
            }

            @Override
            public ExecutionConfig getExecutionConfig() {
                return null;
            }

            @Override
            public ClassLoader getUserCodeClassLoader() {
                return null;
            }

            @Override
            public <V, A extends Serializable> void addAccumulator(String s, Accumulator<V, A> accumulator) {

            }

            @Override
            public <V, A extends Serializable> Accumulator<V, A> getAccumulator(String s) {
                return null;
            }

            @Override
            public Map<String, Accumulator<?, ?>> getAllAccumulators() {
                return null;
            }

            @Override
            public IntCounter getIntCounter(String s) {
                return null;
            }

            @Override
            public LongCounter getLongCounter(String s) {
                return null;
            }

            @Override
            public DoubleCounter getDoubleCounter(String s) {
                return null;
            }

            @Override
            public Histogram getHistogram(String s) {
                return null;
            }

            @Override
            public Set<ExternalResourceInfo> getExternalResourceInfos(String s) {
                return null;
            }

            @Override
            public boolean hasBroadcastVariable(String s) {
                return false;
            }

            @Override
            public <RT> List<RT> getBroadcastVariable(String s) {
                return null;
            }

            @Override
            public <T, C> C getBroadcastVariableWithInitializer(String s, BroadcastVariableInitializer<T, C> broadcastVariableInitializer) {
                return null;
            }

            @Override
            public DistributedCache getDistributedCache() {
                return null;
            }

            @Override
            public <T> ValueState<T> getState(ValueStateDescriptor<T> valueStateDescriptor) {
                return null;
            }

            @Override
            public <T> ListState<T> getListState(ListStateDescriptor<T> listStateDescriptor) {
                return null;
            }

            @Override
            public <T> ReducingState<T> getReducingState(ReducingStateDescriptor<T> reducingStateDescriptor) {
                return null;
            }

            @Override
            public <IN, ACC, OUT> AggregatingState<IN, OUT> getAggregatingState(AggregatingStateDescriptor<IN, ACC, OUT> aggregatingStateDescriptor) {
                return null;
            }

            @Override
            public <UK, UV> MapState<UK, UV> getMapState(MapStateDescriptor<UK, UV> mapStateDescriptor) {
                return null;
            }
        };
    }

    private SinkFunction.Context contextStub() {
        return new SinkFunction.Context() {
            @Override
            public long currentProcessingTime() {
                return 0;
            }

            @Override
            public long currentWatermark() {
                return 0;
            }

            @Override
            public Long timestamp() {
                return null;
            }
        };
    }
}