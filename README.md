# Solution
**Disclaimer**
_I do not consider this code to be production ready, it does not even follow the best practices in some points. Take it as a draft, I am remembering past times with and old friend (Apache Flink) and discovering a new one (InfluxDB). My intention here was to hack a little bit and have fun while coding. I expect you cand find something interesting._

The solution has been done using TDD and the best way to see how it works is looking at the tests. There is an [integration test](https://gitlab.com/alvped/testproject.eng.backend/-/blob/master/src/test/java/io/intellisense/testproject/eng/integration/IntegrationTest.java) that relies in [Testcontainers](https://www.testcontainers.org/modules/databases/influxdb/) to setup an InfluxDB docker image and a [FlinkMiniCluster](https://ci.apache.org/projects/flink/flink-docs-stable/dev/stream/testing.html#testing-flink-jobs) to setup a local Flink envinronment. We can see that the entire pipeline is working e2e, being able to parse a reduced version of the CSV file and, after process all the DataPoints, we assert that we have the corresponding number of elements in the InfluxDB database. This is kind a smoke test and it provides little information about the real behaviour. 

You can see [another test](https://gitlab.com/alvped/testproject.eng.backend/-/blob/master/src/test/java/io/intellisense/testproject/eng/jobs/AnomalyDetectionPipelineTest.java) in which you can see that the pipeline is working fine because most of the DataPoints are not consider anomalous but some of them (less that 10%) are (it prints the anomalous data, so you can check that they are outliers for the corresponding sensor) _(I know Kent Beck, I know, manual verification...)_

**Note**

For the anomaly detection, I have introduced some changes in the proposed algorithm in order to pass [this test](https://gitlab.com/alvped/testproject.eng.backend/-/blob/master/src/test/java/io/intellisense/testproject/eng/function/AnomalyDetectionProcessWindowFunctionTest.java#L15). I assume that we are looking for low outliers, high outliers and normal points and score each point following this rule: 

- 0.0 anomalous score for low outliers
- 0.5 for normal point
- 1.0 for high outliers

and 

- points are low outliers if point_value < Q1 − 1.5x IQR
- points are high outliers if point_value > Q3 + 1.5x IQR
- points are normal if they are between this two thresholds. 

Where Q1 and Q3 are the first and third quartiles of the sample given by 100 consecutive datapoints originated by the same sensor. And IQR is Q3-Q1 (interquartile range)

This behaviour is implemented in [AnomalyScorer](https://gitlab.com/alvped/testproject.eng.backend/-/blob/master/src/main/java/io/intellisense/testproject/eng/function/AnomalyScorer.java) and you can see also the [test](https://gitlab.com/alvped/testproject.eng.backend/-/blob/master/src/test/java/io/intellisense/testproject/eng/function/AnomalyScorerTest.java)

# Objective

Create a streaming data processing pipeline which can test for anomalous data in real-time.

# Context

Anomaly detection is an important part of data processing, bad quality data will lead to bad quality insights. As such
the first part of our data processing does some basic anomaly detection.  
As part of this test project you will create a simple anomaly detection pipeline in Apache Flink using the API it
provides. The pipeline will read data from the provided files, do stream processing to allocate an anomalous score, and
then write the data into InfluxDB.  
Both the original values and the anomalous score should be written to InfluxDB for each sensor reading.

The following dataset can be used for this project: https://www.dropbox.com/s/3ww0xoitwkzaate/TestFile.zip?dl=0  
It is also included in the resources folder of the project.

# Anomaly Detection Method

There are libraries which provide anomaly detection functionality, however many don’t work well for streaming data. The
following algorithm can be used to give a score:

For a sliding window of values (100 values should give ok results)  
Calculate the interquartile range (IQR) for the array  
Based on the IQR, score the value being processed with the following:  
 _I have changed, see below_


# Constraints

The project should be provided in a Git repository such as on gitlab.com  
It is expected it will be in Java using Maven to build the project  
InfluxDB is available as a Docker image  
Instructions on how to run the project should be provided
