package io.intellisense.testproject.eng.function;

import io.intellisense.testproject.eng.model.DataPoint;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;

import java.time.Instant;

public class CsvRowToDataPointFlatMap implements FlatMapFunction<Row, DataPoint> {

    @Override
    public void flatMap(Row row, Collector<DataPoint> collector) {
        final String timestampField = (String) row.getField(0);
        Instant instant = Instant.parse(timestampField);

        for (int i = 1; i <= 10; i++) {
            double value = Double.parseDouble((String) row.getField(i));
            String sensor = "Sensor-" + i;
            DataPoint dataPoint = DataPoint.builder().sensor(sensor).value(value).timestamp(instant).build();
            collector.collect(dataPoint);
        }
    }
}
