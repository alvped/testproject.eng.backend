package io.intellisense.testproject.eng.function;

import io.intellisense.testproject.eng.model.DataPoint;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow;
import org.apache.flink.util.Collector;

public class AnomalyDetectionProcessWindowFunction
        extends ProcessWindowFunction<DataPoint, DataPoint, String, GlobalWindow> {

    @Override
    public void process(
            String key, Context context, Iterable<DataPoint> windowedDataPoints, Collector<DataPoint> collector) {

        DescriptiveStatistics windowStatistics = new DescriptiveStatistics(100);
        windowedDataPoints.forEach(dataPoint -> windowStatistics.addValue(dataPoint.getValue()));

        AnomalyScorer anomalyScorer = AnomalyScorer.builder()
                .firstQuartile(windowStatistics.getPercentile(25))
                .thirdQuartile(windowStatistics.getPercentile(75))
                .build();

        windowedDataPoints.forEach(dataPoint -> {
            anomalyScorer.score(dataPoint);
            collector.collect(dataPoint);
        });
    }
}
