package io.intellisense.testproject.eng.utils;

import io.intellisense.testproject.eng.model.DataPoint;
import io.intellisense.testproject.eng.sink.influxdb.InfluxDBSink;
import org.apache.flink.api.java.utils.ParameterTool;

import java.util.HashMap;
import java.util.Map;

public class SinkFactory {

    public static InfluxDBSink<DataPoint> createInfluxDbSink(int influxDBRunningPort) {
        Map<String, String> properties = new HashMap<>();
        properties.put("influxdb.url", "http://localhost:" + influxDBRunningPort);
        properties.put("influxdb.username", "admin");
        properties.put("influxdb.password", "admin");
        properties.put("influxdb.dbName", "anomaly_detection");

        return new InfluxDBSink<>(ParameterTool.fromMap(properties));
    }
}
