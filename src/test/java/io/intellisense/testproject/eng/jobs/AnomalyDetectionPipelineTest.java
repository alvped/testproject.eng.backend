package io.intellisense.testproject.eng.jobs;

import io.intellisense.testproject.eng.model.DataPoint;
import io.intellisense.testproject.eng.utils.AnomalyReport;
import org.apache.flink.runtime.testutils.MiniClusterResourceConfiguration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.test.util.MiniClusterWithClientResource;
import org.apache.flink.types.Row;
import org.junit.ClassRule;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class AnomalyDetectionPipelineTest {

    @ClassRule
    public static MiniClusterWithClientResource flinkCluster = new MiniClusterWithClientResource(
            new MiniClusterResourceConfiguration.Builder()
                    .setNumberSlotsPerTaskManager(2)
                    .setNumberTaskManagers(1)
                    .build());


    @Test
    public void tesAnomalyDetectionPipeline() throws Exception {
        CollectSink.values.clear();

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(2);

        final DataStream<Row> sourceStream = AnomalyDetectionJob.getDataStream(env, "sensor-data-integration-test.csv");

        AnomalyDetectionPipeline.with(sourceStream, new CollectSink());

        env.execute();

        assertEquals(1000, CollectSink.values.size());
        CollectSink.values.forEach(element -> assertNotNull(element.getAnomalousScore()));
        AnomalyReport anomalyReport = AnomalyReport.of(CollectSink.values);

        anomalyReport.assertThatMostOfElementAreNotAnomalous();
        anomalyReport.printAnomalies();
    }

    // create a testing sink
    private static class CollectSink extends RichSinkFunction<DataPoint> {

        // must be static
        public static final List<DataPoint> values = Collections.synchronizedList(new ArrayList<>());

        @Override
        public void invoke(DataPoint value, Context context) {
            values.add(value);
        }
    }
}