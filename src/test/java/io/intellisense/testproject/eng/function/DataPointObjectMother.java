package io.intellisense.testproject.eng.function;

import io.intellisense.testproject.eng.model.DataPoint;

import java.time.Instant;
import java.util.Random;

public class DataPointObjectMother {

    private static final Random random = new Random();

    public static DataPoint dataPointForNormalDistribution(int mean, int variance) {
        double value = random.nextGaussian() * variance + mean;
        return dataPointWithValue(value);
    }

    public static DataPoint dataPointWithValue(Double value) {
        return DataPoint.builder().timestamp(Instant.now()).sensor("sensorId").value(value).build();
    }

    public static DataPoint.DataPointBuilder fromInstant(Instant instant) {
        return DataPoint.builder().timestamp(instant);
    }
}
