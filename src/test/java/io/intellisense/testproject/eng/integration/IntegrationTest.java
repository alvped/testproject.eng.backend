package io.intellisense.testproject.eng.integration;

import io.intellisense.testproject.eng.jobs.AnomalyDetectionJob;
import io.intellisense.testproject.eng.jobs.AnomalyDetectionPipeline;
import io.intellisense.testproject.eng.utils.SinkFactory;
import org.apache.flink.api.common.JobExecutionResult;
import org.apache.flink.runtime.testutils.MiniClusterResourceConfiguration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.test.util.MiniClusterWithClientResource;
import org.apache.flink.types.Row;
import org.influxdb.InfluxDB;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.junit.ClassRule;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.InfluxDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Testcontainers
class IntegrationTest {

    @ClassRule
    public static MiniClusterWithClientResource flinkCluster = new MiniClusterWithClientResource(
            new MiniClusterResourceConfiguration.Builder()
                    .setNumberSlotsPerTaskManager(2)
                    .setNumberTaskManagers(1)
                    .build());

    @Container
    public InfluxDBContainer influxDbContainer =
            new InfluxDBContainer(DockerImageName.parse("influxdb:1.8"))
                    .withAdmin("admin")
                    .withAdminPassword("admin")
                    .withDatabase("anomaly_detection");

    @Test
    public void integrationTest() throws Exception {
        int influxDBRunningPort = influxDbContainer.getMappedPort(8086);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        final DataStream<Row> sourceStream = AnomalyDetectionJob.getDataStream(env, "sensor-data-integration-test.csv");
        AnomalyDetectionPipeline.with(sourceStream, SinkFactory.createInfluxDbSink(influxDBRunningPort));

        final JobExecutionResult jobResult = env.execute("Anomaly Detection Job");
        assertEquals(1000, (int) jobResult.getAccumulatorResult("InfluxDBSink-recordsIn"));
        assertEquals(1000, (int) jobResult.getAccumulatorResult("InfluxDBSink-recordsOut"));

        InfluxDB influxDB = influxDbContainer.getNewInfluxDB();
        QueryResult queryResult = influxDB.query(new Query("SELECT COUNT(\"value\") FROM datapoints", "anomaly_detection"));

        assertTrue(queryResult.toString().contains("1000.0"));
    }
}