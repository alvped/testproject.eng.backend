package io.intellisense.testproject.eng.jobs;

import io.intellisense.testproject.eng.function.AnomalyDetectionProcessWindowFunction;
import io.intellisense.testproject.eng.function.CsvRowToDataPointFlatMap;
import io.intellisense.testproject.eng.model.DataPoint;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.types.Row;

public class AnomalyDetectionPipeline {

    public static void with(DataStream<Row> sourceStream, SinkFunction<DataPoint> sink) {
        sourceStream
                .flatMap(new CsvRowToDataPointFlatMap())
                .keyBy(DataPoint::getSensor)
                .countWindow(100)
                .process(new AnomalyDetectionProcessWindowFunction())
                .addSink(sink);
    }
}
