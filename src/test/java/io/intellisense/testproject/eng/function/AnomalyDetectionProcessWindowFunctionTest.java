package io.intellisense.testproject.eng.function;

import io.intellisense.testproject.eng.model.DataPoint;
import io.intellisense.testproject.eng.utils.AnomalyReport;
import org.apache.flink.api.common.functions.util.ListCollector;
import org.junit.jupiter.api.RepeatedTest;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AnomalyDetectionProcessWindowFunctionTest {

    @RepeatedTest(10)
    void givenANormalDistributionThenSomeAnomaliesShouldBeDetectedButMostValuesShouldBeNotAnomalous() {
        AnomalyDetectionProcessWindowFunction scorer = new AnomalyDetectionProcessWindowFunction();
        ArrayList<DataPoint> windowedElements = new ArrayList<>();
        ArrayList<DataPoint> processedElements = new ArrayList<>();
        ListCollector<DataPoint> collector = new ListCollector<>(processedElements);

        for (int i = 0; i <= 97; i++) {
            // We generate some values using a normal distribution of mean 500 and variance 100
            windowedElements.add(DataPointObjectMother.dataPointForNormalDistribution(500, 100));
        }

        // We add two anomalies, to test that at least two are detected
        DataPoint lowOutlier = DataPointObjectMother.dataPointWithValue(12.0);
        DataPoint highOutlier = DataPointObjectMother.dataPointWithValue(10000.00);

        windowedElements.add(lowOutlier);
        windowedElements.add(highOutlier);

        scorer.process("sensorId", null, windowedElements, collector);

        AnomalyReport.of(processedElements).assertThatMostOfElementAreNotAnomalous();

        assertEquals(0.0, lowOutlier.getAnomalousScore(), 0.001);
        assertEquals(1.0, highOutlier.getAnomalousScore(), 0.001);
    }
}