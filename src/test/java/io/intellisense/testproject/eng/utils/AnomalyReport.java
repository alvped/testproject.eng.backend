package io.intellisense.testproject.eng.utils;

import io.intellisense.testproject.eng.model.DataPoint;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor(staticName = "of")
public class AnomalyReport {

    final List<DataPoint> processedElements;

    public void assertThatMostOfElementAreNotAnomalous() {
        long numberOfLowOutliers = processedElements.stream().filter(x -> x.getAnomalousScore() <= 0.0).count();
        long numberOfHighOutliers = processedElements.stream().filter(x -> x.getAnomalousScore() >= 1.0).count();
        long numberOfNormalPoints = processedElements.size() - numberOfLowOutliers - numberOfHighOutliers;

        assert (double) numberOfNormalPoints / processedElements.size() > .9;
    }

    public void printAnomalies() {
        processedElements.stream()
                .filter(x -> x.getAnomalousScore() < .49 || x.getAnomalousScore() > 0.9)
                .forEach(System.out::println);
    }
}
